import { Application } from "express";
import HomeController from "./controllers/HomeController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) =>
    {
        HomeController.about(req, res);
    });
}
